import ast
import msvcrt
import os
import shutil
import sys

import xmltodict
from dict2xml import dict2xml


os.chdir(os.path.dirname(sys.argv[0]))


def validation():
    message = None
    if len(sys.argv) < 2:
        message = (
            "Missing argument! The current character name must be provided."
        )
    elif not os.path.dirname(os.getcwd()).endswith("\\Windower\\addons"):
        message = (
            "The program must be placed in the 'addons' folder of Windower, "
            "in is own folder "
            "(...\\Windower\\addons\\Treasury_manager\\"
            "FFXI_treasury_manager.exe)"
        )
    elif not os.path.basename(os.getcwd()) == "Treasury_manager":
        message = (
            "The program must be placed in a folder named: "
            "'Treasury_manager' "
            "(...\\Windower\\addons\\Treasury_manager\\"
            "FFXI_treasury_manager.exe)"
        )
    elif "findAll" not in os.listdir(".."):
        message = (
            "The 'findAll' addon is mandatory. "
            "Please install and configure it."
        )
    elif "Treasury" not in os.listdir(".."):
        message = (
            "The 'Treasury' addon is mandatory. "
            "Please install and configure it."
        )
    return message


def load_settings(name):
    print("Loading settings...")
    settings = load_xml("data\\settings.xml")
    if name.lower() not in settings["settings"].keys():
        settings["settings"].setdefault(
            name.lower(), {"slips": "", "exceptions": None}
        )
        write_settings_xml(settings)
    return settings["settings"][name.lower()]


def load_treasury_settings(name):
    print("Loading Treasury data...")
    settings = load_xml("..\\Treasury\\data\\settings.xml")
    if name.lower() not in settings["settings"].keys():
        settings["settings"].setdefault(
            name.lower(), {"AutoDrop": "true", "Drop": ""}
        )
    return settings


def get_slips_to_parse(settings):
    slips_str = [
        v for k, v in settings.items() if k == "slips" and v is not None
    ]
    if slips_str:  # if "slips" is None, slips_str is an empty list
        return [
            int(slip)
            for slip in slips_str[0].split(",")
            if slip != ""
        ]
    else:
        return slips_str


def get_exceptions(settings):
    exception_str = [
        v for k, v in settings.items() if k == "exceptions" and v is not None
    ]
    if exception_str:  # if "exceptions" is None, it's an empty list
        return [
            exception
            for exception in exception_str[0].split(",")
            if exception != ""
        ]
    else:
        return exception_str


def load_xml(path):
    with open(path, "r") as f:
        data = f.read()
    xml_data = xmltodict.parse(data)
    return xml_data


def write_settings_xml(data):
    file = "data\\settings.xml"
    xml = dict2xml(data, indent=" "*4)

    lines = xml.split("\n")
    new_lines = []
    for line in lines:
        if "None" in line:
            new_lines.append(line.replace("None", ""))
        else:
            new_lines.append(line)
    xml = "\n".join(new_lines)

    shutil.copy2(file, file.replace(".xml", "_backup.xml"))
    with open(file, "w") as f:
        f.write("<?xml version=\"1.1\" ?>\n")
        f.write(xml)


def write_treasury_xml(data):
    print("Updating Treasury data...")
    file = "..\\Treasury\\data\\settings.xml"
    xml = dict2xml(data, indent=" "*4)

    lines = xml.split("\n")
    new_lines = []
    for line in lines:
        if (
            line == "        <Drop>None</Drop>"
            or line == "        <Drop></Drop>"
        ):
            new_lines.append("        <Drop />")
        elif (
            line == "        <Lot>None</Lot>"
            or line == "        <Lot></Lot>"
        ):
            new_lines.append("        <Lot />")
        elif (
            line == "        <Pass>None</Pass>"
            or line == "        <Pass></Pass>"
        ):
            new_lines.append("        <Pass />")
        else:
            new_lines.append(line)
    xml = "\n".join(new_lines)

    shutil.copy2(file, file.replace(".xml", "_backup.xml"))
    with open(file, "w") as f:
        f.write("<?xml version=\"1.1\" ?>\n")
        f.write(xml)


def get_character_list():
    filepath = "..\\findAll\\data"
    files = [f[:-4] for f in os.listdir(filepath) if f.endswith(".lua")]
    return files


def load_character_data(name):
    print("Loading findAll data for {}...".format(name))
    source = os.path.abspath(os.path.join("..\\findAll\\data", name + ".lua"))
    with open(source, "r") as f:
        data = f.read()
    data = data.replace("return ", "")
    data = data.replace('["', '"')
    data = data.replace('"] = ', '":')
    data = ast.literal_eval(data)
    return data


def get_selected_slips_data(slips, character_data):
    keys = [
        key
        for key in character_data.keys()
        if key.startswith("slip") and int(key.strip("slip ")) in slips
    ]
    dict_list = [i for i in [character_data[key] for key in keys]]
    ids = [int(k) for d in dict_list for k, v in d.items() if v >= 1]
    return ids


def load_res():
    print("Loading Windower resource file for items...")
    res_file = "..\\..\\res\\items.lua"
    with open(res_file, "r", encoding="utf-8") as f:
        data = f.read()
    data = data.replace(
        "-- Automatically generated file: Items\n\nreturn ", ""
    )
    data = data.replace(
        ', {"id", "en", "ja", "enl", "jal", "category", "flags", "stack", '
        '"targets", "type", "cast_time", "jobs", "level", "races", "slots", '
        '"cast_delay", "max_charges", "recast_delay", "shield_size", '
        '"damage", "delay", "skill", "item_level", "superior_level"}\n',
        ""
    )
    data = data.replace("--[[", '"""')
    data = data.replace("]]", '"""')
    data = data.replace("    [", "")
    data = data.replace("] = ", ":")
    data = data.replace("id=", '"id":')
    data = data.replace("en=", '"en":')
    data = data.replace("ja=", '"ja":')
    data = data.replace("enl=", '"enl":')
    data = data.replace("jal=", '"jal":')
    data = data.replace("category=", '"category":')
    data = data.replace("flags=", '"flags":')
    data = data.replace("stack=", '"stack":')
    data = data.replace("targets=", '"targets":')
    data = data.replace("type=", '"type":')
    data = data.replace("cast_time=", '"cast_time":')
    data = data.replace("jobs=", '"jobs":')
    data = data.replace("superior_level=", '"superior_level":')
    data = data.replace("item_level=", '"item_level":')
    data = data.replace("level=", '"level":')
    data = data.replace("races=", '"races":')
    data = data.replace("slots=", '"slots":')
    data = data.replace("recast_delay=", '"recast_delay":')
    data = data.replace("cast_delay=", '"cast_delay":')
    data = data.replace("max_charges=", '"max_charges":')
    data = data.replace("shield_size=", '"shield_size":')
    data = data.replace("damage=", '"damage":')
    data = data.replace("delay=", '"delay":')
    data = data.replace("skill=", '"skill":')
    data = data.split('\n"""\n')[0]
    data = ast.literal_eval(data)
    return data


def get_gear_names(res, ids, exceptions):
    names = [res[key]["en"] for key in ids if res[key]["en"] not in exceptions]
    return names


def update_treasury_settings(name, treasury_settings, gear_names):
    data = gear_names
    legacy_data = treasury_settings["settings"][name.lower()].get("Drop", "")
    if legacy_data:
        legacy_data = legacy_data.split(",")
        data = legacy_data + data
    data = list(set(data))  # remove duplicates
    data.sort()
    data_str = ",".join(data)
    treasury_settings["settings"][name.lower()]["Drop"] = data_str
    return treasury_settings


def main():
    if validation() is not None:
        print("ERROR - {}".format(validation()))
        raise Exception
    name = sys.argv[1]
    characters = get_character_list()
    if name not in characters:
        print(
            "ERROR - You must run findAll on '{}' "
            "to create the initial data.".format(name)
        )
        raise Exception
    settings = load_settings(name)
    slips = get_slips_to_parse(settings)
    exceptions = get_exceptions(settings)
    character_data = load_character_data(name)
    slips_data = get_selected_slips_data(slips, character_data)
    res = load_res()
    gear_names = get_gear_names(res, slips_data, exceptions)
    treasury_settings = load_treasury_settings(name)
    updated_treasury_settings = update_treasury_settings(
        name, treasury_settings, gear_names
    )
    write_treasury_xml(updated_treasury_settings)
    print("Job successfully completed.")
    print("Press any key to continue . . .")
    msvcrt.getch()


if __name__ == "__main__":
    main()
