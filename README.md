# Treasury Manager

A tool that populate the "*Drop*" list of the *Treasury* addon, with the items stored in *Porter Moogle* slips.

<sup>The user have to specify the slips he wants to use in the *settings.xml* of Treasury Manager.</sup>

## Getting Started

* Download the files, and move them to a folder named *Treasury_manager* in *...\Windower\addons\\*

* Load the addon, within the game, with the following command: *//lua load Treasury_manager*

* Run the addon a first time (this will create the required data in the settings file), with the following command: *//tm update*  

* Open the settings file (*...\Windower\addons\Treasury_manager\data\settings.xml*) and fill the *exceptions* and *slips* section.

Example:
```
<settings>
    <your_character_name>
        <slips>6,12</slips>
        <exceptions>Assassin's Armlets,Twilight Knife</exceptions>
    </your_character_name>
</settings>
```

<sup>The *slips* section is used to specify a list of slips that will be scanned and added to the *Drop* list of *Treasury*.</sup>

<sup>The *exceptions* section is used to specify a list of items that will **not** be added to the *Drop* list of *Treasury*.</sup>

* Run the addon, with the following command: *//tm update*

* Reload the *Treasury* addon, with the following command: *//lua reload Treasury*

### Prerequisites

* The *Treasury* addon must be installed.

  * The user must load the *Treasury* addon, within the game, at least one time to generate the "*...\Windower\addons\Treasury\data\settings.xml*" file.

* The *findAll* addon must be installed.

  * The user must load the *findAll* addon, within the game and on the same character, at least one time to generate the "*...\Windower\addons\findAll\data\character_name.lua*" file.

## Authors

* **Benoit Boulanger** - *Initial work* - [Benbou](https://gitlab.com/benbou)

See also the list of [contributors](https://gitlab.com/benbou/ffxi-treasury-manager/-/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Thanks to the Windower team for *findAll* and *Treasury*.
