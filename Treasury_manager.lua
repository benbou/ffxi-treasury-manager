_addon.name='Treasury Manager'
_addon.author='Benbou'
_addon.version='0.1'
_addon.commands={'tm'}

require('os')

windower.register_event('addon command',function(...)
    cmd = {...}
    if cmd[1] == 'help' then
        local chat = windower.add_to_chat
        local color = string.color
        chat(1,'Treasury Manager - Command List:')
        chat(207,'//tm '..color('update',166,160)..' --Apply slips data to Treasury settings.')
        chat(207,'//tm '..color('help',166,160)..' --Display this.')
    elseif cmd[1] == 'update' then
        player = windower.ffxi.get_player()
        --windower.add_to_chat(1,'TEST********')
        --windower.add_to_chat(1,player.name)
        local script_path = script_path()
        windower.add_to_chat(1,script_path)
        os.execute('python '..script_path..'Treasury_manager.py '..player.name)
    end
end)

function script_path()
   local str = debug.getinfo(2, "S").source:sub(2)
   return str:match("(.*[/\\])")
end